/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dowhilekalkylator;

import java.util.Scanner;
import java.util.Random;
/**
 *
 * @author Erik Lundstedt
 *2018-01-11
 */
public class DoWhileKalkylator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
	// TODO code application logic here


	        // skapar variabeln input med värdet new Scanner System.in
	        Scanner input = new Scanner(System.in);
	        //göör det samma men för random (mathrandom)

	        // skapa basvariabler här
							String blank = "                    " ;
	//            String blank = "********************" ;    //debug
							String keepGoing ="return to menu? (1/0) ";
							int firstnum;
							int secondnum;
							int ans;
							int i = 0 ;
	        // skapa programm-variabler här




int runMenu = (1);


	//skriver menyn på skärmen
	System.out.println(blank+"  WELCOME");
	System.out.println("                  calkylator 5.0");//programmnamn
	do {
	 if(i!=0) {
			System.out.println(blank);
			System.out.println(blank);
			System.out.println(blank);
			System.out.println(blank);
			System.out.println(blank);
		}
		System.out.println(blank);
		System.out.println(blank);
		System.out.println(blank);
		System.out.println(blank);
		System.out.println(blank);
		System.out.println(blank);
		System.out.println(blank+"press 1 for + ");
		System.out.println(blank+"press 2 for - ");
		System.out.println(blank+"press 3 for * ");
		System.out.println(blank+"press 4 for / ");
		System.out.println(blank+"press 5 to exit ");
		System.out.println(blank+i);
	//komentera ut EN av kommande två

	//int menuChoice=(5); //1 2 3 4 eller 5

	int menuChoice = input.nextInt();
	System.out.println(blank);

	switch (menuChoice)
		{
			case 1:
			{
				System.out.println(blank + "first number:");
				firstnum = input.nextInt();
				System.out.println(blank + "second number:");
				secondnum = input.nextInt();
				ans = firstnum + secondnum;
				System.out.println(blank + firstnum + " + " + secondnum + " = " + ans);
				System.out.print(blank + keepGoing);
				runMenu=input.nextInt();
			break;
			}
			case 2:
			{
				System.out.println(blank + "first number:");
				firstnum = input.nextInt();
				System.out.println(blank + "second number:");
				secondnum = input.nextInt();
				ans = firstnum - secondnum;
				System.out.println(blank + firstnum + " - " + secondnum + " = " + ans);
				System.out.print(blank + keepGoing);
				runMenu=input.nextInt();
			break;
			}
			case 3:
			{
				System.out.println(blank + "first number:");
				firstnum = input.nextInt();
				System.out.println(blank + "second number:");
				secondnum = input.nextInt();
				ans = firstnum * secondnum;
				System.out.println(blank + firstnum + " * " + secondnum + " = " + ans);
				System.out.print(blank + keepGoing);
				runMenu=input.nextInt();
			break;
			}
			case 4:
			{
				System.out.println(blank + "first number:");
				firstnum = input.nextInt();
				System.out.println(blank + "second number:");
				secondnum = input.nextInt();
				ans = firstnum / secondnum;
				System.out.println(blank + firstnum + " / " + secondnum + " = " + ans);
				System.out.print(blank + keepGoing);
				runMenu=input.nextInt();
			break;
			}
			case 5:
			{
				runMenu = (0);
				break;
			}
		}
		i++;
	}while (runMenu == 1);
}
